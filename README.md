# Multiple safe A1111 instances on Docker

This repository contains a minimal setup to run a simplified, SFW (Safe For Work) setup of [Automatic1111 Stable Diffusion WebUI](https://github.com/AUTOMATIC1111/stable-diffusion-webui) using docker on a NVIDIA multi-GPU machine.

It uses 2 extensions [\[1\]](https://github.com/w-e-w/sd-webui-nudenet-nsfw-censor)[\[2\]](https://github.com/aria1th/sd-webui-nsfw-filter) to automatically filter, blur and pixelate unsafe material. The default model [DreamShaperXL_Lightning-SFW](https://huggingface.co/Lykon/dreamshaper-xl-lightning/blob/main/DreamShaperXL_Lightning-SFW.safetensors) is primarily a safe model, but still has some NSFW capability.



## Building and running
Building the [Dockerfile](Dockerfile) takes about 5 minutes. The build happens automatically when running the [docker-compose.yaml](docker-compose.yaml).

```
docker compose -f docker-compose.yml up --build
```

Alternative see [run.sh](run.sh).

By default the `docker compose` will begin with a download process, that fetches the DreamShaperXL model. This download will also take about 5 minutes. Please wait patiently. There is no progress view. If you want to check the download progress, there should be a file in the automatically mounted folder `./models/.cache/huggingface/hub/models--Lykon--dreamshaper-xl-lightning/blobs/` which should grow to 6.4GB. The file will automatically disappear and be moved to `./models/Stable-diffusion/DreamShaperXL_Lightning-SFW.safetensors` once the download is finished.

Then (or if the file exists) the first WebUI conainer will spawn on port `7860` and do automatic setup processes. The first start will take a bit longer. The default configuration of this container will allow to change settings. Settings are stored in the [config.json](`config.json`) and [ui-config.json](`ui-config.json`). Those files currently contain a easy to use preset for SDXL Lightning models.

**Please change the line maked with `UNSAFE` in the [docker-compose.yaml](docker-compose.yaml) and uncomment the `SAFE` line instead. This is important before taking this online.** The `UNSAFE` line is only meant to be used for easier changing of settings by an administrator before hosting.

Once the first container is running, the second container (or more, accordingly to settings) will spawn on port `7861`. It will use the the setup that's allready done from the first container, because it shares all files and doesn't allow editing of files.

To run more than 2 instances, you can copy the whole `sdwebui-2:`-Block in the [docker-compose.yaml](docker-compose.yaml). Give it another name, change the port and set the GPU ID(s) at `device_ids: ['1']`. The default setting are supposed to run one container on GPU0 and one container on GPU1. This requires 2 GPUs. You can run multiple containers on a single GPU and you can run more containers on more GPUs. Each container needs about 8GB VRAM, so you should be able to run 3 containers on a 24GB GPU like a NVIDIA RTX 3090 or better **but** I recomment only running 2 instances on 24GB, e.g. to account for extra space left to run a user interface on the host machine. 
You can also assign multiple GPUs per container, but this is mostly just used for parallel image generation in a single instance. This increases total VRAM usage, because each GPU needs needs to load the weights individually.

## Maintainance
### Updating
The [Dockerfile](Dockerfile) is not particularly version specific. It in the future it might be required to upgrade the base container accordingly to a newer tag on the [NGC catalog](https://catalog.ngc.nvidia.com/orgs/nvidia/containers/pytorch). All other packages will be aquired on their latest version or whatever [A1111 SD WebUI requires](https://github.com/AUTOMATIC1111/stable-diffusion-webui/blob/master/requirements.txt). 

### Extensions
If you need to add aditional extensions, I recommend to add them to the [Dockerfile](Dockerfile). See examples in the Dockerfile.

### Models
To replace the model or add aditional models, see the command used in `model-downloader` in [docker-compose.yml](docker-compose.yml) or just download SafeTensors-files manually and place under `./models/Stable-diffusion/`.

